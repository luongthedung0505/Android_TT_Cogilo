package com.example.khong.logincogilo;

import android.support.v4.media.session.MediaControllerCompat;


import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by khong on 7/8/2017.
 */

public interface RestInterface {
   // String url = "https://cogilo.com/api/login";
    String url = "http://api.androidhive.info/contacts/";
    //"https://www.cogilo.com/api/";
   // http://cogilo.com/api/login HTTP/1.1
    @FormUrlEncoded
    @POST("/login")
    void Login(@Field("email") String email,
               @Field("pass") String pass, Callback<LoginModel> cb);


    @FormUrlEncoded
    @POST("/signup")
    void SingUp(@Field("name") String name, @Field("email") String email,
                @Field("pass") String pass, Callback<LoginModel> pm);
}
