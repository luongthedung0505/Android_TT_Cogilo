package com.example.khong.logincogilo;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.LoginFilter;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity  implements OnClickListener{
    private EditText mPasswordView,mEmailView;
    private Button btn;
    private TextView txt;
    private final String ServerUrl = "https://cogilo.com/api/login";   // địa chỉ api của sever
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mEmailView = (EditText) findViewById(R.id.email);
        txt = (TextView) findViewById(R.id.Tev);
        mPasswordView = (EditText) findViewById(R.id.password);
        btn = (Button) findViewById(R.id.btnn);
        btn.setOnClickListener(this);
    }
    //onclick cho nút btn
    @Override
    public void onClick(View view) {
        Gson gs = new Gson(); // thư viện gson
        LoginModel lg = new LoginModel(mEmailView.getText().toString(),mPasswordView.getText().toString(),mEmailView.getText().toString());
        // tạo 1 class LoginModel tạo constructor
        String jsonlg = gs.toJson(lg);  // gửi chuỗi loginModel lên gson
        txt.setText(jsonlg);  // in ra kiểm tra chuỗi gson
        new login().execute(ServerUrl,jsonlg); // tạo class login bên dưới kia kìa ... :v
        // execute Trả về truenếu đối tượng đầu tiên trả về truy vấn là một ResultSetđối tượng. Sử dụng phương pháp này nếu truy vấn có thể trả lại một hoặc nhiều ResultSetđối tượng. Truy lục các ResultSetđối tượng được trả về từ truy vấn bằng cách liên tục gọi Statement.getResultSet.
    }



    class login extends AsyncTask<String,Void,Boolean>{
        private ProgressDialog dialog = new ProgressDialog(LoginActivity.this);
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage("Đang đăng nhập...");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Boolean doInBackground(String... params) {
        //viết chức năng đăng nhập
            boolean bn = false;
            try {
                URL url = new URL(params[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setConnectTimeout(600000);
                connection.setReadTimeout(60000);
                connection.setDoOutput(true);
                connection.setDoInput(true);
                connection.setRequestMethod("POST"); // phương thức truy nhập
                connection.setRequestProperty("Content-Type","application/json"); // kiểu truy nhập
                String jsonLogin = params[1];
                OutputStream outputStream = connection.getOutputStream();
                outputStream.write(jsonLogin.getBytes());
                outputStream.close();
                if(connection.getResponseCode() == HttpURLConnection.HTTP_OK){
                        bn = true;
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bn;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            dialog.dismiss();
        if(aBoolean){
            Toast.makeText(LoginActivity.this,"Đăng nhập thành công",Toast.LENGTH_LONG).show();
            Intent i = new Intent(LoginActivity.this,Menu.class);
            startActivity(i);
        }
        else {
            Toast.makeText(LoginActivity.this,"Sai mật khẩu hoặc tài khoản.",Toast.LENGTH_LONG).show();
        }
        }
    }

}

