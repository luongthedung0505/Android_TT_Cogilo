package com.example.khong.logincogilo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.iid.FirebaseInstanceId;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import self.yue.cogilo.R;
import self.yue.cogilo.data.local.Role;
import self.yue.cogilo.data.local.User;
import self.yue.cogilo.data.server.CogiloApiProvider;
import self.yue.cogilo.data.server.CogiloApiProvider2;
import self.yue.cogilo.ui.base.BaseActivity;
import self.yue.cogilo.ui.base.BaseActivityBehaviour;
import self.yue.cogilo.ui.base.ShowableContent;
import self.yue.cogilo.ui.main.MainActivity;
import self.yue.cogilo.utils.CacheHelper;
import self.yue.cogilo.utils.CommonConstants;
import self.yue.cogilo.utils.CommonUtils;
import self.yue.cogilo.utils.FirebaseUtil;
import self.yue.cogilo.utils.FontUtil;

/**
 * Created by dongc on 4/3/2017.
 */

public class LoginActivity extends BaseActivity implements BaseActivityBehaviour, ShowableContent {
    private static final int REQUEST_REGISTER = 95;

    private EditText mEditEmail;
    private EditText mEditPassword;
    private ProgressDialog mProgressDialog;
    private ScrollView mScrollView;

    private CallbackManager mCallbackManager;
    private TwitterAuthClient mTwitterAuthClient;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_login;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViews();

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_REGISTER) {
            if (resultCode == RESULT_OK) {
                mEditEmail.setText(data.getStringExtra(CommonConstants.EXTRA_EMAIL));
                mEditPassword.setText(data.getStringExtra(CommonConstants.EXTRA_PASSWORD));
            }
        } else {
            if (mCallbackManager != null)
                mCallbackManager.onActivityResult(requestCode, resultCode, data);
            if (mTwitterAuthClient != null)
                mTwitterAuthClient.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public Intent newIntent(Class activity) {
        return new Intent(this, activity);
    }

    @Override
    public Intent newIntent(Class activity, Bundle data) {
        Intent intent = new Intent(this, activity);
        intent.putExtra(CommonConstants.EXTRA_BUNDLE_NAME, data);
        return intent;
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void initViews() {
        mEditEmail = (EditText) findViewById(R.id.edit_email);
        mEditPassword = (EditText) findViewById(R.id.edit_password);
        mScrollView = (ScrollView) findViewById(R.id.scroll_view);

        mScrollView.setVerticalScrollBarEnabled(false);
        mScrollView.setHorizontalScrollBarEnabled(false);

        Glide.with(this).load(R.drawable.img_light).into((ImageView) findViewById(R.id.img_overlay));

        findViewById(R.id.btn_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = mEditEmail.getText().toString();
                final String password = mEditPassword.getText().toString();
                if (TextUtils.isEmpty(email)) {
                    showToast(getString(R.string.empty_email));
                } else if (TextUtils.isEmpty(password)) {
                    showToast(getString(R.string.empty_password));
                } else {
                    mProgressDialog.show();
                    login(email, password);
                }
            }
        });

        findViewById(R.id.btn_sign_up).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(newIntent(SignUpActivity.class), REQUEST_REGISTER);
            }
        });

        findViewById(R.id.btn_login_facebook).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCallbackManager == null)
                    mCallbackManager = CallbackManager.Factory.create();
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this,
                        Arrays.asList("email", "public_profile"));
                LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        handleLoginWithFacebook(loginResult);
                    }

                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onError(FacebookException error) {
                        showToast(error.getMessage());
                    }
                });
            }
        });

        findViewById(R.id.btn_login_google).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showToast(getString(R.string.coming_soon));
            }
        });

        findViewById(R.id.btn_login_twitter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTwitterAuthClient == null)
                    mTwitterAuthClient = new TwitterAuthClient();
                mTwitterAuthClient.authorize(LoginActivity.this, new Callback<TwitterSession>() {
                    @Override
                    public void success(Result<TwitterSession> result) {
                        handleLoginWithTwitter(result);
                    }

                    @Override
                    public void failure(TwitterException exception) {
                        showToast(exception.getMessage());
                    }
                });
            }
        });

        setFont();
    }

    private void setFont() {
        mEditEmail.setTypeface(FontUtil.getInstance().getMainLightFont());
        mEditPassword.setTypeface(FontUtil.getInstance().getMainLightFont());
        ((TextView) findViewById(R.id.text_sign_in_with))
                .setTypeface(FontUtil.getInstance().getMainLightFont());
        ((TextView) findViewById(R.id.btn_forget_password))
                .setTypeface(FontUtil.getInstance().getMainLightFont());
        ((TextView) findViewById(R.id.btn_sign_up))
                .setTypeface(FontUtil.getInstance().getMainLightFont());
        ((Button) findViewById(R.id.btn_login))
                .setTypeface(FontUtil.getInstance().getCustomFont(this, "fonts/iCiel_Alina.otf"));
    }

    private void login(final String email, final String password) {
        CogiloApiProvider.getInstance().login(email, password,
                new CogiloApiProvider.OnServerResponseListener<User>() {
                    @Override
                    public void onSuccess(User data) {
                        // Cache current user
                        CacheHelper.getInstance().saveAccount(email, password);
                        CacheHelper.getInstance().saveUser(data);

                        FirebaseUtil.getInstance().getFirebaseAuth().signInAnonymously()
                                .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        mProgressDialog.dismiss();
                                        if (task.isSuccessful()) {
                                            FirebaseUtil.getInstance()
                                                    .refreshToken(FirebaseInstanceId.getInstance().getToken());

                                            startActivity(newIntent(MainActivity.class));
                                            finish();
                                        }
                                    }
                                });
                    }

                    @Override
                    public void onFail(Throwable t) {
                        mProgressDialog.dismiss();
                        showToast(t.getMessage());
                    }
                });
    }

    private void handleLoginWithFacebook(LoginResult loginResult) {
        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            final String email = object.getString("email");
                            final String firstName = object.getString("first_name");
                            final String lastName = object.getString("last_name");
                            final String socialId = object.getString("id");

                            mProgressDialog.show();

                            CogiloApiProvider2.getInstance().loginSocial(email, socialId, firstName, lastName,
                                    new CogiloApiProvider.OnServerResponseListener<String>() {
                                        @Override
                                        public void onSuccess(String data) {
                                            CacheHelper.getInstance().saveToken(data);


                                            FirebaseUtil.getInstance().getFirebaseAuth().signInAnonymously()
                                                    .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                                            mProgressDialog.dismiss();
                                                            if (task.isSuccessful()) {
                                                                FirebaseUtil.getInstance()
                                                                        .refreshToken(FirebaseInstanceId.getInstance().getToken());

                                                                startActivity(newIntent(MainActivity.class));
                                                                finish();
                                                            }
                                                        }
                                                    });
                                        }

                                        @Override
                                        public void onFail(Throwable t) {
                                            mProgressDialog.dismiss();
                                            showToast(t.getMessage());
                                        }
                                    });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
        );

        Bundle parameters = new Bundle();
        parameters.putString("fields", "email,first_name,last_name");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void handleLoginWithTwitter(Result<TwitterSession> result) {
        TwitterSession session = result.data;
        final String name = session.getUserName();
        final String email = "" + session.getUserId() + "@gmail.com";
        final String password = "" + session.getUserId();

        mProgressDialog.show();

        CogiloApiProvider.getInstance().login(email, password, new CogiloApiProvider.OnServerResponseListener<User>() {
            @Override
            public void onSuccess(User data) {
                mProgressDialog.dismiss();
                CacheHelper.getInstance().saveAccount(email, password);
                CacheHelper.getInstance().saveUser(data);

                FirebaseUtil.getInstance().getFirebaseAuth().signInAnonymously()
                        .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                mProgressDialog.dismiss();
                                if (task.isSuccessful()) {
                                    FirebaseUtil.getInstance()
                                            .refreshToken(FirebaseInstanceId.getInstance().getToken());

                                    startActivity(newIntent(MainActivity.class));
                                    finish();
                                }
                            }
                        });
            }

            @Override
            public void onFail(Throwable t) {
                if (CommonUtils.isNetworkAvailable(LoginActivity.this)) {
                    CogiloApiProvider.getInstance().register(
                            name,
                            email,
                            password,
                            Role.ROLE_USER,
                            new CogiloApiProvider.OnServerResponseListener<String>() {
                                @Override
                                public void onSuccess(String data) {
                                    mProgressDialog.dismiss();
                                    login(email, password);
                                }

                                @Override
                                public void onFail(Throwable t) {
                                    mProgressDialog.dismiss();
                                    showToast(t.getMessage());
                                }
                            });
                } else {
                    mProgressDialog.dismiss();
                    showToast(t.getMessage());
                }
            }
        });
    }
}
