package com.example.khong.logincogilo;

import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import self.yue.cogilo.data.local.CallLog;
import self.yue.cogilo.data.local.Image;
import self.yue.cogilo.data.local.Option;
import self.yue.cogilo.data.local.User;
import self.yue.cogilo.data.server.models.ServerResponse;
import self.yue.cogilo.data.server.models.ServerResponse2;
import self.yue.cogilo.data.server.models.UserResult;

/**
 * Created by dongc on 4/18/2017.
 */

public interface CogiloService {
    @POST("login")
    Call<ServerResponse<UserResult>> login(@Body HashMap<String, String> data);

    @POST("login")
    Call<ResponseBody> loginSocial(@Body HashMap<String, String> data);

    @POST("register")
    Call<ServerResponse<User>> register(@Body HashMap<String, String> data);

    @GET("catalogues")
    Call<ResponseBody> getCatalogues(@Header("Authorization") String token,
                                     @Query("per_page") int itemsPerPage,
                                     @Query("page") int page);

    @GET("catalogues")
    Call<ServerResponse2<List<Option>>> getCategoryOptions(@Header("Authorization") String token);

    @GET("mentors")
    Call<ServerResponse2<List<User>>> getMentorsByCatergory(@Header("Authorization") String token,
                                                            @Query("catalogueId") int categoryId);

    @POST("image/upload")
    @FormUrlEncoded
    Call<ServerResponse<Image>> uploadImage(@Header("Authorization") String token, @Field("image") String imageBase64);

    @PUT("users/update")
    Call<ServerResponse<User>> updateUser(@Header("Authorization") String token, @Body JsonObject data);

    @GET("mentors/{userId}")
    Call<ServerResponse<User>> getUserById(@Header("Authorization") String token, @Path("userId") long userId);

    @POST("call_logs")
    Call<ResponseBody> createCallLog(@Header("Authorization") String token, @Body JsonObject data);

    @GET("call_logs")
    Call<ServerResponse2<List<CallLog>>> getCallLogs(@Header("Authorization") String token,
                                                     @Query("page") int page,
                                                     @Query("orderDirection") String order);

    @GET("mentor_by_sip_number/{sipNumber}")
    Call<ServerResponse<User>> getUserBySipNumber(@Header("Authorization") String token,
                                                  @Path("sipNumber") String sipNumber);

    @GET("mentors")
    Call<ServerResponse2<List<User>>> searchUser(@Header("Authorization") String token,
                                                 @Query("catalogueId") int catalogueId,
                                                 @Query("search") String search);

    @GET("mentors/status")
    Call<ServerResponse2<List<User>>> getMentorsByCategory(@Header("Authorization") String token,
                                                           @Query("catalogueId") int catalogueId);
}
